package es.cipfpbatoi.ad;

import com.sun.tools.jdeprscan.scan.Scan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {


        try {
            /**
             * La clase buffered reader se puede sustituir por la clase
             * Scanner.
             * 
             * BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
             * 
             * Es equivalente a:
             * 
             * Scanner sc = new Scanner(System.in)
             * String cadena = sc.nextLine();
             * 
             */
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String cadena = "";
            cadena = br.readLine();

            while (!cadena.equals("stop")) {
                int n = (int) (Math.random() * 10 + 1);
                cadena = br.readLine();
                System.out.println(n);
            }
            if (cadena.equals("stop")) {
                br.close();
            }
        } catch (Exception e) {
        }
    }

   /* public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);
        while (!teclado.nextLine().equals("stop")){
            int numero = (int) (Math.random() * 10 + 1);
            System.out.println(numero);
        }
        System.out.println("El usuario ha introducido stop.");
    }*/
}
