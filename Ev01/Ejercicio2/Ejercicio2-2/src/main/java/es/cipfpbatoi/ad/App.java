package es.cipfpbatoi.ad;


import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class App {

    private static String FICHERO1 = "src/main/resources/output.txt";

    static void setUp(){
        File fichero1 = new File(App.FICHERO1);
        if (fichero1.exists()){
            fichero1.delete();
        }
        try {
            fichero1.createNewFile();
        }catch (IOException e){
            System.err.println("No se ha podido generar el fichero para redirigir el rasultado del comando, pero se mostrará por pantalla.");
        }
    }

    public static void main(String[] args) {
        String comando = "java -jar ../Ejercicio2/target/Ejercicio2-1.0-SNAPSHOT.jar";
        //linea de codigo para separar las instrucciones del comando y que lo coja correctamente.
        List<String> argsList = new ArrayList<>(Arrays.asList(comando.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argsList);
        //pb.redirectOutput(ProccessBuilder.Redirect.INHERIT);
        //pb.redirectInput(ProcessBuilder.Redirect.INHERIT);

        try {
            Process random10 = pb.start();

            /*
            * random10.waitFor();*/

            Scanner random10Scanner = new Scanner(random10.getInputStream());
            //envio de palabras al proceso

            /**
             * Se podría haber utilizado la misma estrategia 
             * que en Ejercicio2 de generar un método
             */
            OutputStream os = random10.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner sc = new Scanner(System.in);
            String line = sc.nextLine();
            FileWriter salida = new FileWriter(FICHERO1);
            while (!line.equals("stop")) {
                bw.write(line);
                bw.newLine();
                bw.flush();
                System.out.println(random10Scanner.nextLine());
                salida.write(random10Scanner.nextLine());
            }
            salida.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

/**
 * En tu intento, no se exactamente que error da,
 * pero podrías utilizar redirectIO y esperar al proceso.
 * 
 * Saludos.
 */

            //ESTO ES MI INTENTO Y NO VA.
           /* BufferedReader bufReader = lecturaProceso(random10);

            FileWriter salida = new FileWriter(FICHERO1);
            while ((line = bufReader.readLine()) != null){
                System.out.println(line);
                salida.write(line+"\n");
            }
            salida.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static BufferedReader lecturaProceso(Process proceso) {
        InputStream inSteam = proceso.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inSteam);
        BufferedReader bufReader = new BufferedReader(inputStreamReader);
        return bufReader;
    }*/

