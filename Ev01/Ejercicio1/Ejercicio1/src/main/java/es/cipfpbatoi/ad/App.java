package es.cipfpbatoi.ad;

import java.io.*;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class App
{
    private static String FICHERO1 = "src/main/resources/output.txt";

    static void setUp(){
        File fichero1 = new File(App.FICHERO1);
        if (fichero1.exists()){
            fichero1.delete();
        }
        try {
            fichero1.createNewFile();
        }catch (IOException e){
            System.err.println("No se ha podido generar el fichero para redirigir el rasultado del comando, pero se mostrará por pantalla.");
        }
    }

    public static void main(String[] args) {
        /**
         * Creo que los argumentos no pueden ser
         * menores de 0.
         */
        if (args.length <= 0){
            System.err.println("Necesito un comando para ejecutar");
            System.exit(-1);
        }

        ProcessBuilder pb = new ProcessBuilder(args);
        try {
            Process proceso = pb.start();
            /**
             * Se tenía que haber utilizado el método
             * proceso.waitfor(time,unit)
             */
            TimeUnit.SECONDS.sleep(2);
            BufferedReader bufReader = lecturaProceso(proceso);
            System.out.println("El output que muestra el comando " + Arrays.toString(args) + " es: ");
            String line;
            FileWriter salida = new FileWriter(FICHERO1);
            while ((line = bufReader.readLine()) != null){
                System.out.println(line);
                salida.write(line+"\n");
                /**
                 * Creo que es mejor utilizar el println si
                 * está disponible, porque así, los saltos de
                 * línea los escribe la máquina virtual en función
                 * de la plataforma
                 */
            }
            salida.close();
        } catch (IOException e) {
            System.err.println("Se ha producido un error al ejecutar el proceso");
        }
         catch (InterruptedException e) {
            System.err.println("El tiempo de espera se ha agotado");
        }
    }

    /**
     * La idea de utilizar este método está genial, 
     * faltaría poner que lanza determinadas excepciones (throws)
     * y al haberlo utilizado así, lo ideal es utilizar 
     * 
     * try (BufferedReader bufReader = lecturaProceso(proceso);)
     * {
     *    Código ...
     * } catch (Excepciones e){
     * 
     * }
     * 
     * De esta manera nos ahorramos bastante código,
     * de todas maneras todo esto lo verás en acceso a datos.
     */
    private static BufferedReader lecturaProceso(Process proceso) {
        InputStream inSteam = proceso.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inSteam);
        BufferedReader bufReader = new BufferedReader(inputStreamReader);
        return bufReader;
    }
}
